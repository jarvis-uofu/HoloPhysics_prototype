﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License. See LICENSE in the project root for license information.

using Academy.HoloToolkit.Unity;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR.WSA.Input;

namespace HoloToolkit.Unity
{
    /// <summary>
    /// GestureManager provides access to several different input gestures, including Tap and Manipulation.
    /// </summary>
    /// <remarks>
    /// When a tap gesture is detected, GestureManager uses GazeManager to find the currently focused object.
    /// GestureManager then sends a message to that game object.
    /// 
    /// Using Manipulation requires subscribing the the ManipulationStarted events and then querying
    /// information about the manipulation gesture via ManipulationOffset and ManipulationHandPosition.
    /// 
    /// Editor and Companion App Input can also be used by assigning a keyboard select key and
    /// using the right mouse button to select the currently focused object.
    /// 
    /// Using Gestures with mouse is currently not supported.
    /// </remarks>
    [RequireComponent(typeof(GazeManager))]
    public partial class GestureManager : Singleton<GestureManager>
    {
        #region Delegate Events

        /// <summary>
        /// Occurs when a manipulation gesture has started.
        /// </summary>
        /// <param name="sourceKind">The Interaction Source Kind that started the event.</param>
        public delegate void ManipulationStartedDelegate(InteractionSourceKind sourceKind);
        public event ManipulationStartedDelegate OnManipulationStarted;

        /// <summary>
        /// Occurs when a manipulation gesture ended as a result of user input.
        /// </summary>
        /// <param name="sourceKind">The Interaction Source Kind that completed the event.</param>
        public delegate void ManipulationCompletedDelegate(InteractionSourceKind sourceKind);
        public event ManipulationCompletedDelegate OnManipulationCompleted;

        /// <summary>
        /// Occurs when a manipulated gesture ended as a result of some other condition.
        /// (e.g. the hand being used for the gesture is no longer visible).
        /// </summary>
        /// <param name="sourceKind">The Interaction Source Kind that cancelled the event.</param>
        public delegate void ManipulationCanceledDelegate(InteractionSourceKind sourceKind);
        public event ManipulationCanceledDelegate OnManipulationCanceled;

        #endregion

        #region Public Properties


        [Tooltip("Object that will be launched")]
        public GameObject LaunchableObject = null;

        /// <summary>
        /// To select even when a hologram is not being gazed at,
        /// set the override focused object.
        /// If its null, then the gazed at object will be selected.
        /// </summary>
        public GameObject OverrideFocusedObject { get; set; }

        /// <summary>
        /// Gets the currently focused object, or null if none.
        /// </summary>
        public GameObject FocusedObject { get; private set; }

        /// <summary>
        /// Whether or not a manipulation gesture is currently in progress
        /// </summary>
        public bool ManipulationInProgress { get; private set; }

        /// <summary>
        /// The offset of the input source from its position at the beginning of 
        /// the currently active manipulation gesture, in world space.
        /// Valid if a manipulation gesture is in progress.
        /// </summary>
        public Vector3 ManipulationOffset { get; private set; }

        public Vector3 ManipulationPosition { get; private set; }


        /// <summary>
        /// InteractionSourceDetected tracks the interaction detected state.
        /// Returns true if the list of tracked interactions is not empty.
        /// </summary>
        public bool InteractionSourceDetected
        {
            get { return trackedInteractionSource.Count > 0; }
        }

        /// <summary>
        /// InteractionSourcePressed track the interaction pressed state.
        /// Returns true if the list of pressed interactions is not empty.
        /// </summary>
        public bool InteractionSourcePressed
        {
            get { return pressedInteractionSource.Count > 0; }
        }

        // Currently active gesture recognizer.
        public GestureRecognizer ActiveRecognizer { get; private set; }

        #endregion

        /// <summary>
        /// Key to press that will select the currently focused object.
        /// </summary>
        public KeyCode keyboardSelectKey = KeyCode.Space;

        /// <summary>
        /// Enumerated Mouse Buttons.
        /// </summary>
        public enum MouseButton
        {
            Left = 0,
            Right = 1,
            Middle = 2
        }

        public enum Mode
        {
            None = 0,
            Rotating = 1,
            Moving = 2,
            Scaling = 3,
            Select = 4
        }

        public Mode SelectMode { get { return Mode.Select; } }
        public Mode RotatingMode { get { return Mode.Rotating; } }
        public Mode MovingMode { get { return Mode.Moving; } }
        public Mode ScalingMode { get { return Mode.Scaling; } }
        public Mode NoneMode { get { return Mode.None; } }

        public Mode CurrentMode { get; set; }

        /// <summary>
        /// Mouse button to press that will select the current focused object.
        /// </summary>
        public MouseButton MouseSelectButton = MouseButton.Right;

        private GestureRecognizer gestureRecognizer;

        /// <summary>
        /// Used for rotating the arrow
        /// </summary>
        public GestureRecognizer NavigationRecognizer { get; private set; }

        public GestureRecognizer ManipulationRecognizer { get; private set; }

        public static GestureRecognizer ManipulationRecognizerTest { get; set; }

        private InteractionSourceState currentInteractionSourceState;

        private HashSet<uint> trackedInteractionSource = new HashSet<uint>();

        private HashSet<uint> pressedInteractionSource = new HashSet<uint>();

        private bool hasRecognitionStarted;

        private GameObject lastFocusedObject;

        public bool IsNavigating { get; private set; }
        public bool IsManipulating { get; private set; }
        public bool IsLaunching { get; private set; }

        public Vector3 NavigationPosition { get; private set; }

        private void Awake()
        {
            CurrentMode = SelectMode;

            NavigationRecognizer = new GestureRecognizer();
            SubscribeToNavigation(NavigationRecognizer);
            NavigationRecognizer.StartCapturingGestures();

            // Create a new GestureRecognizer. 
            // Sign up for manipulation events.
            ManipulationRecognizer = new GestureRecognizer();
            SubscribeToManipulation(ManipulationRecognizer);
        }

        private void SubscribeToManipulation(GestureRecognizer ManipulationRecognizer)
        {
            // ManipulationTranslate only recognizes hand gesture translations, but not Clicker translations.
            ManipulationRecognizer.SetRecognizableGestures(GestureSettings.ManipulationTranslate);

            // Subscribe to our manipulation events.
            ManipulationRecognizer.ManipulationStartedEvent += ManipulationRecognizer_ManipulationStartedEvent;
            ManipulationRecognizer.ManipulationUpdatedEvent += ManipulationRecognizer_ManipulationUpdatedEvent;
            ManipulationRecognizer.ManipulationCompletedEvent += ManipulationRecognizer_ManipulationCompletedEvent;
            ManipulationRecognizer.ManipulationCanceledEvent += ManipulationRecognizer_ManipulationCanceledEvent;
        }

        private void UnsubscribeToManipulation()
        {
            ManipulationRecognizer.StopCapturingGestures();
            ManipulationRecognizer.ManipulationStartedEvent -= ManipulationRecognizer_ManipulationStartedEvent;
            ManipulationRecognizer.ManipulationUpdatedEvent -= ManipulationRecognizer_ManipulationUpdatedEvent;
            ManipulationRecognizer.ManipulationCompletedEvent -= ManipulationRecognizer_ManipulationCompletedEvent;
            ManipulationRecognizer.ManipulationCanceledEvent -= ManipulationRecognizer_ManipulationCanceledEvent;

        }

        private void SubscribeToNavigation(GestureRecognizer navigationRecognizer)
        {
            NavigationRecognizer.SetRecognizableGestures(GestureSettings.Tap | GestureSettings.NavigationX | GestureSettings.NavigationRailsY | GestureSettings.NavigationRailsZ);

            // Register for the TappedEvent with the NavigationRecognizer_TappedEvent function.
            NavigationRecognizer.TappedEvent += NavigationRecognizer_TappedEvent;
            // Register for the NavigationStartedEvent with the NavigationRecognizer_NavigationStartedEvent function.
            NavigationRecognizer.NavigationStartedEvent += NavigationRecognizer_NavigationStartedEvent;
            // Register for the NavigationUpdatedEvent with the NavigationRecognizer_NavigationUpdatedEvent function.
            NavigationRecognizer.NavigationUpdatedEvent += NavigationRecognizer_NavigationUpdatedEvent;
            // Register for the NavigationCompletedEvent with the NavigationRecognizer_NavigationCompletedEvent function. 
            NavigationRecognizer.NavigationCompletedEvent += NavigationRecognizer_NavigationCompletedEvent;
            // Register for the NavigationCanceledEvent with the NavigationRecognizer_NavigationCanceledEvent function. 
            NavigationRecognizer.NavigationCanceledEvent += NavigationRecognizer_NavigationCanceledEvent;
        }

        private void UnsubscribeToNavigation()
        {
            // Unregister the Tapped and Navigation events on the NavigationRecognizer.
            NavigationRecognizer.TappedEvent -= NavigationRecognizer_TappedEvent;

            NavigationRecognizer.NavigationStartedEvent -= NavigationRecognizer_NavigationStartedEvent;
            NavigationRecognizer.NavigationUpdatedEvent -= NavigationRecognizer_NavigationUpdatedEvent;
            NavigationRecognizer.NavigationCompletedEvent -= NavigationRecognizer_NavigationCompletedEvent;
            NavigationRecognizer.NavigationCanceledEvent -= NavigationRecognizer_NavigationCanceledEvent;
        }


        public void ResetGestureRecognizer()
        {
            Transition(gestureRecognizer);
        }

        public void ChangeGestureRecognizer()
        {
            Transition(ManipulationRecognizer);
        }

        public void ChangeMode(Mode mode)
        {
            CurrentMode = mode;
        }


        /// <summary>
        /// Transition to a new GestureRecognizer.
        /// </summary>
        /// <param name="newRecognizer">The GestureRecognizer to transition to.</param>
        public void Transition(GestureRecognizer newRecognizer)
        {
            if (newRecognizer == null)
            {
                return;
            }

            if (ActiveRecognizer != null)
            {
                if (ActiveRecognizer == newRecognizer)
                {
                    return;
                }

                ActiveRecognizer.CancelGestures();
                ActiveRecognizer.StopCapturingGestures();
            }

            newRecognizer.StartCapturingGestures();
            ActiveRecognizer = newRecognizer;
        }

        #region Navigation Recognizer
        private void NavigationRecognizer_TappedEvent(InteractionSourceKind source, int tapCount, Ray ray)
        {
            GameObject focusedObject = InteractibleManager.Instance.FocusedGameObject;
           
            if (IsLaunching)
            {
                IsLaunching = false;
                print("IsLaunching is: " + IsLaunching);
            }
            else if (focusedObject != null)
            {
                if (focusedObject.Equals(LaunchableObject))
                {
                    IsLaunching = true;
                    print("IsLaunching is: " + IsLaunching);
                }
                focusedObject.SendMessageUpwards("OnSelect");
            }
        }

        private void NavigationRecognizer_NavigationStartedEvent(InteractionSourceKind source, Vector3 relativePosition, Ray ray)
        {
            if (HandsManager.Instance.FocusedGameObject != null)
            {
                // Set IsNavigating to be true.
                IsNavigating = true;

                // Set NavigationPosition to be relativePosition.
                NavigationPosition = relativePosition;
            }
        }

        private void NavigationRecognizer_NavigationUpdatedEvent(InteractionSourceKind source, Vector3 relativePosition, Ray ray)
        {
            if (HandsManager.Instance.FocusedGameObject != null)
            {

                // Set IsNavigating to be true.
                IsNavigating = true;

                // Set NavigationPosition to be relativePosition.
                NavigationPosition = relativePosition;
                string PerformMode = CurrentMode == Mode.Scaling ? "PerformScaling" : "PerformRotation";

                HandsManager.Instance.FocusedGameObject.SendMessageUpwards(PerformMode, NavigationPosition);
            }
        }

        private void NavigationRecognizer_NavigationCompletedEvent(InteractionSourceKind source, Vector3 relativePosition, Ray ray)
        {
            // Set IsNavigating to be false.
            IsNavigating = false;
        }

        private void NavigationRecognizer_NavigationCanceledEvent(InteractionSourceKind source, Vector3 relativePosition, Ray ray)
        {
            // Set IsNavigating to be false.
            IsNavigating = false;
        }
        #endregion

        /// <summary>
        /// Raised when the gesture manager recognizes that a manipulation has begun.
        /// </summary>
        /// <param name="source">Input Source Kind.</param>
        /// <param name="cumulativeDelta">Cumlulative Data.</param>
        /// <param name="headRay">The Ray from the users forward direction.</param>
        private void ManipulationRecognizer_ManipulationStartedEvent(InteractionSourceKind source, Vector3 position, Ray headRay)
        {
            if (HandsManager.Instance.FocusedGameObject != null)
            {
                IsManipulating = true;
                ManipulationPosition = position;

                HandsManager.Instance.FocusedGameObject.SendMessageUpwards("PerformManipulationStart", position);
            }
        }

        /// <summary>
        /// Raised when the gesture manager recognizes that a manipulation has been updated.
        /// </summary>
        /// <param name="source">Input Source Kind.</param>
        /// <param name="cumulativeDelta">Cumlulative Data.</param>
        /// <param name="headRay">The Ray from the users forward direction.</param>
        private void ManipulationRecognizer_ManipulationUpdatedEvent(InteractionSourceKind source, Vector3 position, Ray headRay)
        {
            print("Focused object is: " + HandsManager.Instance.FocusedGameObject);
            if (HandsManager.Instance.FocusedGameObject != null)
            {
                IsManipulating = true;

                ManipulationPosition = position;
                HandsManager.Instance.FocusedGameObject.SendMessageUpwards("PerformManipulationUpdate", position);
            }
        }

        /// <summary>
        /// Raised when the gesture manager recognizes that a manipulation has completed.
        /// </summary>
        /// <param name="source">Input Source Kind.</param>
        /// <param name="cumulativeDelta">Cumlulative Data.</param>
        /// <param name="headRay">The Ray from the users forward direction.</param>
        private void ManipulationRecognizer_ManipulationCompletedEvent(InteractionSourceKind source, Vector3 cumulativeDelta, Ray headRay)
        {
            IsManipulating = false;

        }

        /// <summary>
        /// Raised when the gesture manager recognizes that a manipulation has been canceled.
        /// </summary>
        /// <param name="source">Input Source Kind.</param>
        /// <param name="cumulativeDelta">Cumlulative Data.</param>
        /// <param name="headRay">The Ray from the users forward direction.</param>
        private void ManipulationRecognizer_ManipulationCanceledEvent(InteractionSourceKind source, Vector3 cumulativeDelta, Ray headRay)
        {
            IsManipulating = false;

        }

        private void OnDestroy()
        {
            UnsubscribeToNavigation();
            UnsubscribeToManipulation();
        }
    }
}