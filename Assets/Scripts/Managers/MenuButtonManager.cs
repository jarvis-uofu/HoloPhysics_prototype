﻿using UnityEngine;
using System.Collections.Generic;

public class MenuButtonManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		List<Transform> buttons = new List<Transform>();
		foreach (Transform child in transform) {
			MenuButton button = child.GetComponent<MenuButton>();
			if (button != null)
				buttons.Add(child);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
