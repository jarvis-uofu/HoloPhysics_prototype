﻿using UnityEngine;
using System.Collections;
using HoloToolkit.Unity;
using UnityEngine.VR.WSA.Input;
using UnityEngine.Windows.Speech;
using System.Collections.Generic;
using System;
using System.Linq;

public class TestManager : MonoBehaviour {

    float expandAnimationCompletionTime;
    // Store a bool for whether our astronaut model is expanded or not.
    bool isModelExpanding = false;

    // KeywordRecognizer object.
    KeywordRecognizer keywordRecognizer;

    // Defines which function to call when a keyword is recognized.
    delegate void KeywordAction(PhraseRecognizedEventArgs args);
    Dictionary<string, KeywordAction> keywordCollection;

    void Start()
    {
        keywordCollection = new Dictionary<string, KeywordAction>();

        // Add keyword to start manipulation.
        keywordCollection.Add("Move", Move);
        keywordCollection.Add("Rotate", Rotate);

        // Initialize KeywordRecognizer with the previously added keywords.
        keywordRecognizer = new KeywordRecognizer(keywordCollection.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        keywordRecognizer.Start();
    }

    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        KeywordAction keywordAction;

        if (keywordCollection.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke(args);
        }
    }

    private void Rotate(PhraseRecognizedEventArgs args)
    {
        throw new NotImplementedException();
    }

    private void Move(PhraseRecognizedEventArgs args)
    {
        //throw new NotImplementedException();
        GestureManager.ManipulationRecognizerTest = new GestureRecognizer();

    }

    // Update is called once per frame
    void Update () {
	
	}

    private void Awake()
    {
        //Register for the evernts 
        //GestureManager.ManipulationRecognizerTest = new GestureRecognizer();
    }
}
