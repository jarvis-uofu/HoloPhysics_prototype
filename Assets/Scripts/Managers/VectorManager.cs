﻿using UnityEngine;
using HoloToolkit.Unity;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Windows.Speech;

public class VectorManager : MonoBehaviour {


    float expandAnimationCompletionTime;
    // Store a bool for whether our astronaut model is expanded or not.
    bool isModelExpanding = false;

    // KeywordRecognizer object.
    KeywordRecognizer keywordRecognizer;

    // Defines which function to call when a keyword is recognized.
    delegate void KeywordAction(PhraseRecognizedEventArgs args);
    Dictionary<string, KeywordAction> keywordCollection;

    void Start()
    {
        keywordCollection = new Dictionary<string, KeywordAction>();

        // Add keyword to start manipulation.
        keywordCollection.Add("Scale", ScaleVector);

        // Initialize KeywordRecognizer with the previously added keywords.
        keywordRecognizer = new KeywordRecognizer(keywordCollection.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        keywordRecognizer.Start();
    }

    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        KeywordAction keywordAction;

        if (keywordCollection.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke(args);
        }
    }

    private void ScaleVector(PhraseRecognizedEventArgs args)
    {
        print("Now Scaling");
        GestureManager.Instance.ChangeMode(GestureManager.Instance.ScalingMode);
        GestureManager.Instance.Transition(GestureManager.Instance.NavigationRecognizer);
    }

 
    public void Update()
    {
        
    }
}
