﻿using HoloToolkit.Unity;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Windows.Speech;

public class AvatarManager : Singleton<AvatarManager>
{
    float expandAnimationCompletionTime;
    // Store a bool for whether our astronaut model is expanded or not.
    bool isModelExpanding = false;

    // KeywordRecognizer object.
    KeywordRecognizer keywordRecognizer;

    // Defines which function to call when a keyword is recognized.
    delegate void KeywordAction(PhraseRecognizedEventArgs args);
    Dictionary<string, KeywordAction> keywordCollection;

    void Start()
    {
        keywordCollection = new Dictionary<string, KeywordAction>();

        // Add keyword to start manipulation.
        keywordCollection.Add("Launch", LaunchAvatar);
        keywordCollection.Add("Move", MoveAvatar);
        keywordCollection.Add("Rotate", RotateAvatar);      

        // Initialize KeywordRecognizer with the previously added keywords.
        keywordRecognizer = new KeywordRecognizer(keywordCollection.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        keywordRecognizer.Start();
    } 

    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        KeywordAction keywordAction;

        if (keywordCollection.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke(args);
        }
    }

    private void RotateAvatar(PhraseRecognizedEventArgs args)
    {
        print("Now Rotating avatar");
        GestureManager.Instance.ChangeMode(GestureManager.Instance.RotatingMode);
        GestureManager.Instance.Transition(GestureManager.Instance.NavigationRecognizer);
    }

    private void MoveAvatar(PhraseRecognizedEventArgs args)
    {
        ChangeToMovingMode();       
    }

    public void ChangeToMovingMode()
    {
        print("Now Manipulating");
        GestureManager.Instance.ChangeMode(GestureManager.Instance.MovingMode);
        GestureManager.Instance.Transition(GestureManager.Instance.ManipulationRecognizer);
    }

    private void LaunchAvatar(PhraseRecognizedEventArgs args)
    {
        print("Now Launching");
        GestureManager.Instance.ChangeMode(GestureManager.Instance.SelectMode);
        GestureManager.Instance.Transition(GestureManager.Instance.NavigationRecognizer);
    }

    public void Update()
    {
       
    }
}