﻿using UnityEngine;
using HoloToolkit.Unity;

/// <summary>
/// GestureAction performs custom actions based on
/// which gesture is being performed.
/// </summary>
public class VectorGestureAction : MonoBehaviour
{

    [Tooltip("Scaling max speed controls amount of scaling.")]
    public float ScalingSenitivity = 10.0f;
    private float scalingFactor;

    public float RotationSensitivity = 10.0f;

    private float rotationFactorX;
    private float rotationFactorY;
    private float rotationFactorZ;

    void Update()
    {
        //PerformRotation();
        //PerformScaling();
    }

    private void PerformRotation(Vector3 NavigationPosition)
    {
        if (GestureManager.Instance.IsNavigating && GestureManager.Instance.CurrentMode == GestureManager.Instance.RotatingMode)
        {
            rotationFactorX = NavigationPosition.x * RotationSensitivity;
            rotationFactorY = NavigationPosition.y * RotationSensitivity;
            rotationFactorZ = NavigationPosition.z * RotationSensitivity;

            transform.Rotate(new Vector3(0, -1 * rotationFactorY, rotationFactorX));
        }
    }

    private void PerformScaling(Vector3 NavigationPosition)
    {
        if (GestureManager.Instance.IsNavigating && GestureManager.Instance.CurrentMode == GestureManager.Instance.ScalingMode)
        {
            Vector3 newLocalScale = transform.localScale + (NavigationPosition);

            float old = transform.localScale.magnitude;
            float newScale = newLocalScale.magnitude;

            print("old: " + old);
            print("new: " + newScale);
            print("dif: " + (newScale - old));

            // Keeps it from growing negatively
            if (newLocalScale.x >= 0 && newLocalScale.y >= 0 && newLocalScale.z >= 0)
            {
                print("local scale" + transform.localScale);
                transform.localScale += newVector3(old- newScale);
            }            
        }
    }
    

    public Vector3 newVector3(float num)
    {
        return new Vector3(num, num, num);
    }
}
