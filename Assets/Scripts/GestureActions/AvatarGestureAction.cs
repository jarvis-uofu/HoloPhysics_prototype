﻿using HoloToolkit.Unity;
using UnityEngine;

/// <summary>
/// GestureAction performs custom actions based on
/// which gesture is being performed.
/// </summary>
public class AvatarGestureAction : MonoBehaviour
{
    [Tooltip("Moving position speed")]
    public float PosittionSpeed = 10.0f;

    [Tooltip("Rotation max speed controls amount of rotation.")]
    public float RotationSensitivity = 10.0f;

    private Vector3 manipulationPreviousPosition;

    private float rotationFactorX;
    private float rotationFactorY;
    private float rotationFactorZ;

    void Update()
    {
        // Don't perform rotation on the avatar, but on vectors
        //PerformRotation();
    }

    private void PerformRotation()
    {
        if (GestureManager.Instance.IsNavigating && GestureManager.Instance.CurrentMode == GestureManager.Instance.RotatingMode)
        {            
            rotationFactorX = GestureManager.Instance.NavigationPosition.x * RotationSensitivity;
            rotationFactorY = GestureManager.Instance.NavigationPosition.y * RotationSensitivity;
            rotationFactorZ = GestureManager.Instance.NavigationPosition.z * RotationSensitivity;

            transform.Rotate(new Vector3(0, -1 * rotationFactorY, rotationFactorX));
        }
    }

    void PerformManipulationStart(Vector3 position)
    {
        manipulationPreviousPosition = position;
    }

    void PerformManipulationUpdate(Vector3 position)
    {
        if (GestureManager.Instance.IsManipulating)
        {
            print("Manipulating");
            Vector3 moveVector = Vector3.zero;       
            moveVector = position - manipulationPreviousPosition;
            manipulationPreviousPosition = position;
            transform.position += moveVector * PosittionSpeed;
        }
    }
}
