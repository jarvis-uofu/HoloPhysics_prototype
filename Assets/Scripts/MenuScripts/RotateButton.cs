﻿using UnityEngine;
using HoloToolkit.Unity;

public class RotateButton : MonoBehaviour, MenuButton
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnSelect()
    {
        ChangeToRotateMode();
    }

    public void ChangeToRotateMode()
    {
		ChangeMode();
    }

	public void ChangeMode() {
        print("Now Rotating avatar");
        GestureManager.Instance.ChangeMode(GestureManager.Instance.RotatingMode);
        GestureManager.Instance.Transition(GestureManager.Instance.NavigationRecognizer);
	}
}
