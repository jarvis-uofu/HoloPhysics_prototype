﻿using UnityEngine;
using HoloToolkit.Unity;

public class ResizeButton : MonoBehaviour, MenuButton
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnSelect()
    {
		ChangeMode();
    }

	public void ChangeMode() {
        print("Now Scaling");
        GestureManager.Instance.ChangeMode(GestureManager.Instance.ScalingMode);
        GestureManager.Instance.Transition(GestureManager.Instance.NavigationRecognizer);
	}
}
