﻿using UnityEngine;
using HoloToolkit.Unity;
using System;

public class MoveButton : MonoBehaviour, MenuButton {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnSelect()
    {
		ChangeMode();
    }

	public void ChangeMode() {
        print("Now Manipulating");
        GestureManager.Instance.ChangeMode(GestureManager.Instance.MovingMode);
        GestureManager.Instance.Transition(GestureManager.Instance.ManipulationRecognizer);
	}
}
