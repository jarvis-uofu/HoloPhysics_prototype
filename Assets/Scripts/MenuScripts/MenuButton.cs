﻿/// <summary>
/// This interface exists just to define what objects are buttons.
/// </summary>
public interface MenuButton {
	void ChangeMode();
}
