﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class NavigationManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () { }

	void OnSelect() {      
		SceneManager.LoadScene(SceneLocations.scenes[this.gameObject.name]);
	}
}
