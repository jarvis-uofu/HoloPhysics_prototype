﻿using UnityEngine;
using System.Collections;

public class P1PhysicsEngine : PhysicsEngine
{
    protected override void StartExtraStuff()
    {
    }

    public override void OnSelect()
    {
        //Vector3 spoofedUserInputVelocity = new Vector3(1f, 0f, 0f);
        totalVelocity = GetInitialVelocity();
 
        rb.velocity = totalVelocity;
        paused = false;
    }

    protected override void OnTriggerEnterExtraStuff(Collider other)
    {
        rb.velocity = Vector3.zero;
        rb.position = GameObject.Find("Goal").transform.position;
    }

    public override void FixedUpdate()
    {
        if (!paused)
        {
            // Update the current magnitude of total velocity
            updateMagnitude(totalVelocityVector, rb.velocity);

            // Update the current angle of total velocity
            updateAngle(totalVelocityHolder, rb.velocity);

            // Update all of the size of the x, y, and z velocity vectors.
            updateXYZ();
        }
    }
}
