﻿using UnityEngine;
using System.Collections;

public class P2PhysicsEngine : PhysicsEngine
{
    GameObject obstacle;
    float forceScale;

    protected override void StartExtraStuff()
    {
        obstacle = GameObject.Find("Obstacle");
        forceScale = 2f;
    }

    public override void OnSelect()
    {
        //Vector3 spoofedUserInputVelocity = new Vector3(1f, 0f, 0f);
        totalVelocity = GetInitialVelocity();
        
        rb.velocity = totalVelocity;
        paused = false;
    }

    protected override void OnTriggerEnterExtraStuff(Collider other)
    {
        rb.velocity = Vector3.zero;
        rb.position = GameObject.Find("Goal").transform.position;
    }

    public override void FixedUpdate()
    {
        if (!paused)
        {
            Vector3 direction = -1 * (obstacle.transform.position - rb.transform.position).normalized;
            float dist = Vector3.Distance(obstacle.transform.position, rb.transform.position);
            float mag = forceScale / (dist * dist);
            totalAcceleration = direction * mag;

            rb.AddForce(totalAcceleration, ForceMode.Acceleration);


            // Update the current magnitude and angle of total velocity
            updateMagnitude(totalVelocityVector, rb.velocity);
            updateAngle(totalVelocityHolder, rb.velocity);

            // Update the current magnitude and angle of total acceleration
            updateMagnitude(totalAccelerationVector, totalAcceleration);
            updateAngle(totalAccelerationHolder, direction);

            // Update all of the size of the x, y, and z velocity vectors.
            updateXYZ();
        }
    }
}

