﻿using UnityEngine;
using System.Collections;
using System;

public class CoordinateTransfom
{
    /// <summary>
    /// Converts cartesian coordinates to spherical coordinates, and spits out the radius, polar, and elevation values
    /// </summary>
    /// <param name="vec"></param>
    /// <param name="radius"></param>
    /// <param name="polar"></param>
    /// <param name="elevation"></param>
    public static void CartesianToSpherical(Vector3 vec, out float radius, out float polar, out float elevation)
    {
        if (vec.x == 0)
            vec.x = Mathf.Epsilon;

        radius = ComputeRadius(vec.x, vec.y, vec.z);
        polar = ComputePolar(vec.x, vec.z);

        if (vec.x < 0)
            polar += Mathf.PI;

        elevation = ComputeElevation(vec.y, radius);
    }   

    /// <summary>
    /// Converts spherical coordinates to Cartesian coordinates, and spits out a Vector3 with the new values.
    /// </summary>
    /// <param name="radius"></param>
    /// <param name="polar"></param>
    /// <param name="elevation"></param>
    /// <param name="vec"></param>
    public static void SphericalToCartesian(float radius, float polar, float elevation, out Vector3 vec)
    {
        float a = radius * Mathf.Cos(elevation);
        vec.x = a * Mathf.Cos(polar);
        vec.y = radius * Mathf.Sin(elevation);
        vec.z = a * Mathf.Sin(polar);
    }

    /// <summary>
    /// Computes the radius given an x, y, and z
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <returns></returns>
    private static float ComputeRadius(float x, float y, float z)
    {
        return Mathf.Sqrt(Mathf.Pow(x, 2)
                    + Mathf.Pow(y, 2)
                    + Mathf.Pow(z, 2));
    }

    /// <summary>
    /// Compute polar value given the x and z axis
    /// </summary>
    /// <param name="x"></param>
    /// <param name="z"></param>
    /// <returns></returns>
    public static float ComputePolar(float x, float z)
    {
         return Mathf.Atan(z / x);
    }

    /// <summary>
    /// Computes the elevation value given the y point and radius
    /// </summary>
    /// <param name="y"></param>
    /// <param name="radius"></param>
    /// <returns></returns>
    private static float ComputeElevation(float y, float radius)
    {
        return Mathf.Asin(y / radius);
    }
}
