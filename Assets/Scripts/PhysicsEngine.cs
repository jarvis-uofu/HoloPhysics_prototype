﻿using UnityEngine;
using System.Collections.Generic;

public abstract class PhysicsEngine : MonoBehaviour
{
    public Rigidbody rb;
    
    public Vector3 totalVelocity;
    public bool paused;

    public List<Vector3> accelerations = new List<Vector3>();
    public Vector3 totalAcceleration;
    public Vector3 grav = new Vector3(0, -1.0f, 0);
    public float scaleFactor = 5; // Magic number in order to scale the vectors to look up to scale, intead of having tiny vectors.
    public float vectorMagOffset = 1f;
	public float velocityScaleFactor = 0.1f;

    protected AvatarCollisionListener avatarCollisionListener;

    // These will be generated when velocity is applied but for the proto they will just be attached already
    public GameObject totalVelocityVector;
    public GameObject totalVelocityHolder;
    public GameObject xVelocityVector;
    public GameObject yVelocityVector;
    public GameObject zVelocityVector;
    public GameObject totalAccelerationVector;
    public GameObject totalAccelerationHolder;
    public GameObject goal;
    public GameObject projectile;

    //static float currentTime;

    // Use this for initialization
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
        rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
        rb.useGravity = false;
        totalVelocity = Vector3.zero;
        totalAcceleration = Vector3.zero;
        paused = true;

        totalVelocityVector = GameObject.Find("TotalVelocity");
        totalVelocityHolder = GameObject.Find("TotalVelocityHolder");
        xVelocityVector = GameObject.Find("XVelocity");
        yVelocityVector = GameObject.Find("YVelocity");
        zVelocityVector = GameObject.Find("ZVelocity");
        totalAccelerationVector = GameObject.Find("TotalAcceleration");
        totalAccelerationHolder = GameObject.Find("TotalAccelerationHolder");
        goal = GameObject.Find("Goal");
        projectile = GameObject.Find("projectile");

        StartExtraStuff();
    }

    protected abstract void StartExtraStuff();

    public abstract void OnSelect();

	public void OnTriggerEnter(Collider other) {
		if (avatarCollisionListener != null)
			avatarCollisionListener.AvatarCollision(other);
		OnTriggerEnterExtraStuff(other);
	}

    public Vector3 GetInitialVelocity()
    {
        float magnitude = totalVelocityVector.transform.localScale.x * velocityScaleFactor;
        Vector3 initialVelocity = magnitude * totalVelocityHolder.transform.forward.normalized;
        print("The magnitude is: " + magnitude);
        return initialVelocity;
    }

    protected abstract void OnTriggerEnterExtraStuff(Collider other);

    void OnCollisionEnter(Collision col)
    {
		Debug.Log("OnCollisionEnter called");
    }

    public abstract void FixedUpdate();

    /// <summary>
    /// Update the magnitude of the given game object. 
    /// </summary>
    /// <param name="gameObject"></param>
    /// <param name="velocity"></param>
    public void updateMagnitude(GameObject gameObject, Vector3 velocity)
    {
        // Multiplying by the scale factor to make it larger because the actual magnitude is too small to see anything interesting.
        float mag = velocity.magnitude * scaleFactor * 1.75f;

        // Updating Total Velocity vector
        gameObject.transform.localScale = new Vector3(mag, mag, mag);
    }

    /// <summary>
    /// Applies the new angle to the vector object given the current velocity of the vector.
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="velocity"></param>
    public void updateAngle(GameObject obj, Vector3 velocity)
    {
        Quaternion rotation = Quaternion.LookRotation(velocity);
        obj.transform.rotation = rotation;
    }

    public void updateXYZ()
    {
        scaleVector(xVelocityVector, rb.velocity.x);
        scaleVector(yVelocityVector, rb.velocity.y);
        scaleVector(zVelocityVector, rb.velocity.z);
    }

    /// <summary>
    /// Change the size of the given vector given the magnitude of the velocity in that direction
    /// </summary>
    /// <param name="vector">The vector to be scaled</param>
    /// <param name="directionMagnitude">The magnitude of the velocity in that direction</param>

    public void scaleVector(GameObject vector, float directionMagnitude)
    {
        float mag = (directionMagnitude * scaleFactor) + vectorMagOffset;
        vector.transform.localScale = new Vector3(mag, mag, mag);
    }

    public void onApplyAcceleration(Vector3 newAcceleration)
    {
        totalAcceleration = totalAcceleration + newAcceleration;
    }

    public void onPause()
    {
        paused = true;
        rb.velocity = Vector3.zero;
    }

    public void onRestart()
    {
        paused = false;
        rb.velocity = totalVelocity;
    }

	public interface AvatarCollisionListener {
		void AvatarCollision(Collider collider);
	}

	public void SetAvatarCollisionListener(AvatarCollisionListener listener) {
		avatarCollisionListener = listener;
	}

	public void RemoveAvatarCollisionListener() {
		avatarCollisionListener = null;
	}

}

