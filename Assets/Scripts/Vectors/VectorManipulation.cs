﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VectorManipulation : MonoBehaviour
{

    Vector3 avatarVec;
    Rigidbody rb;
    TextMesh text;
    Vector3 oldSize;
    Vector3 oldScale;
    GameObject projectile;
    VectorProperties vectorProperties;
    float angle;

    // Use this for initialization
    void Start()
    {
        angle = 0;
    }

    // Update is called once per frame
    void Update()
    {
    }

    /// <summary>
    /// This init method is for when we modify the vector and we need to set its initial 
    /// </summary>
    /// <param name="obj"></param>
    void Init(object obj)
    {
        vectorProperties = obj as VectorProperties;
        transform.position = vectorProperties.Postion;

        Vector3 dir = vectorProperties.Direction;
        float polar = CoordinateTransfom.ComputePolar(dir.x, dir.y);
        transform.RotateAround(vectorProperties.Postion, vectorProperties.Direction, polar);
    }

    /// <summary>
    /// Change scale based on velocity, and angle based on the direction of the vector
    /// 
    /// Takes in a Vector Manipulation object in order to access the velocity and direction.
    /// </summary>
    /// <param name="VectorProperties"></param>
    void ComputeVector(object obj)
    {
        VectorProperties vp = obj as VectorProperties;
        float radius = 0;
        float polar = 0;
        float elevation = 0;
        CoordinateTransfom.CartesianToSpherical(vp.Postion, out radius, out polar, out elevation);
        if (vp != null)
        {
            //Vector3 currentScale = transform.localScale;

            //transform.localScale = new Vector3(vp.Magnitude, vp.Magnitude, vp.Magnitude);r
            transform.localScale = new Vector3(vp.Velocity.magnitude, vp.Velocity.magnitude, vp.Velocity.magnitude);

            // It's vm.Angle + 90 because we want to match the angles in the unit circle. 0 in unity is equal to 90 in unit circle.
            //transform.RotateAround(vp.Postion, Vector3.forward, vp.Angle - 90); // uncomment when done debugging
        }
    }
}
