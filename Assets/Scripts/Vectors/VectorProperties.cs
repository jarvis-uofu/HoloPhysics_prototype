﻿using UnityEngine;

internal class VectorProperties{
    public float Magnitude { get; set; }
    public float Angle { get; set; }
    public Vector3 Direction { get; set; }
    public Vector3 Postion { get; set; }
    public Vector3 Velocity { get; set; }

}
