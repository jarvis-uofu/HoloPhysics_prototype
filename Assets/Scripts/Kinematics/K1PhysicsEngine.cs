﻿using HoloToolkit.Unity;
using UnityEngine;

public class K1PhysicsEngine : PhysicsEngine
{
    protected override void StartExtraStuff()
    {
    }

    public override void OnSelect()
    {
        GameObject focusedObject = InteractibleManager.Instance.FocusedGameObject;
        if (GestureManager.Instance.IsLaunching)// && LaunchableObject != null && focusedObject.Equals(LaunchableObject))
        {
            print("on select called");

            // Changed spoofed input based on user input.
            totalVelocity = GetInitialVelocity();

            rb.velocity = totalVelocity;
        }
    }
   
    protected override void OnTriggerEnterExtraStuff(Collider other)
    {
        rb.velocity = Vector3.zero;
        rb.position = GameObject.Find("Goal").transform.position;	
    }

    public override void FixedUpdate()
    {
        GameObject focusedObject = InteractibleManager.Instance.FocusedGameObject;

        //print("IsLaunching inside physics engine is: " + GestureManager.Instance.IsLaunching);
        if (GestureManager.Instance.IsLaunching)// && LaunchableObject != null && focusedObject.Equals(LaunchableObject))
        {
            // Update the current magnitude of total velocity
            updateMagnitude(totalVelocityVector, rb.velocity);

            // Update the current angle of total velocity
            updateAngle(totalVelocityHolder, rb.velocity);

            // Update all of the size of the x, y, and z velocity vectors.
            updateXYZ();
        }
        else
        {
            rb.velocity = Vector3.zero;
        }
    }
}
