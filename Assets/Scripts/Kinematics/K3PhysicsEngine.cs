﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class K3PhysicsEngine : PhysicsEngine
{
    float time;

    protected override void StartExtraStuff()
    {
    }

    public override void OnSelect()
    {
        rb.AddForce(grav, ForceMode.Acceleration);
        accelerations.Add(grav);
        totalAcceleration = grav;

        //Vector3 spoofedUserInputVelocity = new Vector3(1f, 0.5f, 0f);
        //Vector3 spoofedUserInputVelocity = new Vector3(1.25f, 0f, 0.6f);
        totalVelocity = GetInitialVelocity();

        rb.velocity = totalVelocity;
        time = Time.time;
        paused = false;
    }

    protected override void OnTriggerEnterExtraStuff(Collider other)
    {
        rb.velocity = Vector3.zero;
        totalAcceleration = Vector3.zero;
        accelerations.Remove(grav);

        if (rb != null)
        {
            DestroyImmediate(rb);
        }

        paused = true;

        // Makes the avatar go to the middle of the goal
        rb.position = goal.transform.position;
    }

    public override void FixedUpdate()
    {
        // Update the total velocity vectors scale to reflect the current Velocity
        if (!paused)
        {
            totalVelocity = rb.velocity;
            rb.AddForce(totalAcceleration, ForceMode.Acceleration);

            // Update the current magnitude of total velocity
            updateMagnitude(totalVelocityVector, rb.velocity);

            // Update the current angle of total velocity
            updateAngle(totalVelocityHolder, rb.velocity);

            // Update all of the size of the x, y, and z velocity vectors.
            updateXYZ();
        }
    }
}
