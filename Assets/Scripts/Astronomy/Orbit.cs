﻿using UnityEngine;
using System.Collections;

public class Orbit : MonoBehaviour {
    float timeCount = 0;

    float speed;
    float radiusX;
    float radiusZ;
    GameObject planet;
    Vector3 planetPosition;
    
    // Use this for initialization
    void Start () {
        speed = 0.25f;
		// Radius controls distance and shape of orbit
        radiusX = 2;
        radiusZ = 2;
	}
	
	// Update is called once per frame
	void Update () {
        timeCount += Time.deltaTime * speed;

        planet = GameObject.Find("Earth");
        planetPosition = planet.transform.position;
        
		// Get current orbit position using time
        float x = Mathf.Cos(timeCount) * radiusX + planetPosition.x;
        float y = 0;
        float z = Mathf.Sin(timeCount) * radiusZ + planetPosition.z;

        transform.position = new Vector3(x, y, z);
	}
}
