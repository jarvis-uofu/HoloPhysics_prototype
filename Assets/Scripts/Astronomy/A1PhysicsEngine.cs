﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class A1PhysicsEngine : PhysicsEngine
{
    protected override void StartExtraStuff()
    {
    }

    public override void OnSelect()
    {
        //Vector3 spoofedUserInputVelocity = new Vector3(1f, 0f, 0f);
        totalVelocity = GetInitialVelocity();

        rb.velocity = totalVelocity;
        paused = false;
    }

    protected override void OnTriggerEnterExtraStuff(Collider other)
    {
		Debug.Log("OnTriggerEnter called");
        rb.velocity = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void FixedUpdate()
    {
        if (!paused)
        {
            updateAngle(totalVelocityHolder, rb.velocity);
            updateMagnitude(totalVelocityVector, rb.velocity);

            updateXYZ();
        }
    }
}
