﻿using UnityEngine;
using System.Collections;

public class EarthOrbit : MonoBehaviour
{
    float timeCount = 0;

    float speed;
    float radiusX;
    float radiusZ;
    Vector3 planetPosition;

    // Use this for initialization
    void Start()
    {
        speed = 0.5f;
        // Radius controls distance and shape of orbit
        radiusX = 4f;
        radiusZ = 4f;

        GameObject planet = GameObject.Find("Sun");
        planetPosition = planet.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        timeCount += Time.deltaTime * speed;

        // Get current orbit position using time
        float x = Mathf.Cos(timeCount) * radiusX + planetPosition.x;
        float y = 0;
        float z = Mathf.Sin(timeCount) * radiusZ + planetPosition.z;

        transform.position = new Vector3(x, y, z);
    }
}