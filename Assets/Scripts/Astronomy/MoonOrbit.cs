﻿using UnityEngine;
using System.Collections;

public class MoonOrbit : MonoBehaviour {
    GameObject planet;
    Vector3 planetPosition;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        planet = GameObject.Find("Earth");
        planetPosition = planet.transform.position;
        transform.RotateAround(Vector3.zero, Vector3.up, 20 * Time.deltaTime);
    }
}
