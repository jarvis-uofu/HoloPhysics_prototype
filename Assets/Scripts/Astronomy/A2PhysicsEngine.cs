﻿using UnityEngine;
using System.Collections;
using System;

public class A2PhysicsEngine : PhysicsEngine {

	GameObject sun;
	float forceScale;

    protected override void StartExtraStuff()
    {
        sun = GameObject.Find("Sun");
        forceScale = 2f;
    }

    public override void OnSelect()
    {
        //Vector3 spoofedUserInputVelocity = new Vector3(0.75f, 0f, -0.96f);
        totalVelocity = GetInitialVelocity();

        rb.velocity = totalVelocity;
        paused = false;
    }

    protected override void OnTriggerEnterExtraStuff(Collider other)
    {
		Debug.Log("OnTriggerEnter called");
        rb.velocity = Vector3.zero;
    }

    // Update is called once per frame
    void Update() { }

    public override void FixedUpdate()
    {
		if (!paused) {
			Vector3 direction = (sun.transform.position - rb.transform.position).normalized;
            float dist = Vector3.Distance(sun.transform.position, rb.transform.position);
            float mag = forceScale / (dist*dist);
            totalAcceleration = direction * mag;

			rb.AddForce(totalAcceleration, ForceMode.Acceleration);

            updateAngle(totalVelocityHolder, rb.velocity);
            updateMagnitude(totalVelocityVector, rb.velocity);
            updateAngle(totalAccelerationHolder, direction);
            updateMagnitude(totalAccelerationVector, totalAcceleration);

            updateXYZ();
		}
    }
}
