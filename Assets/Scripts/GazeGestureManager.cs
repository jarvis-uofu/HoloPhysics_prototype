﻿using UnityEngine;
using UnityEngine.VR.WSA.Input;

public class GazeGestureManager : MonoBehaviour {

	public static GazeGestureManager Instance { get; private set; }

	// Hologram that is currently being gazed at.
	public GameObject FocusedObject { get; private set; }

	GestureRecognizer recognizer;

	// Use this for initialization
	void Start () {
		Instance = this;

		recognizer = new GestureRecognizer();
		recognizer.TappedEvent += (source, tapCount, ray) => {
			if (FocusedObject != null) {
				FocusedObject.SendMessageUpwards("OnSelect");
			}
		};
		recognizer.StartCapturingGestures();
	}
	
	// Update is called once per frame
	void Update () {
        GameObject oldFocusedObject = FocusedObject;

		// Do a raycast into the world based on the user's
		// head position and orientation.
		var headPosition = Camera.main.transform.position;
		var gazeDirection = Camera.main.transform.forward;

		RaycastHit hitInfo;
		//if (Physics.Raycast(headPosition, gazeDirection, out hitInfo, 30.0f, SpatialMapping.PhysicsRaycastMask)) {
		if (Physics.Raycast(headPosition, gazeDirection, out hitInfo)) {
			FocusedObject = hitInfo.collider.gameObject;
		}
		else {
			FocusedObject = null;
		}

		if (FocusedObject != oldFocusedObject) {
			recognizer.CancelGestures();
			recognizer.StartCapturingGestures();
		}
	}
}
