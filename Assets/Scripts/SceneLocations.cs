﻿using System.Collections.Generic;

public static class SceneLocations {

	public static Dictionary<string, int> scenes = new Dictionary<string, int>() {
		{ "menu", 0 },
        { "exampleScene", 31 }, // Get rid of this one after testing
        { "kinematics_1", 1 },// Make this one 1 again after done testing
        { "kinematics_2", 2 },
        { "kinematics_3", 3 },
        { "kinematics_4", 4 },
        { "kinematics_5", 5 },
        { "astronomy_1", 6 },
        { "astronomy_2", 7 },
        { "astronomy_3", 8 },
        { "astronomy_4", 9 },
        { "astronomy_5", 10 },
        { "particle_1", 11 },
        { "particle_2", 12 },
        { "particle_3", 13 },
        { "particle_4", 14 },
        { "particle_5", 15 },
        { "tutorial_k1", 16 },
        { "tutorial_k2", 17 },
        { "tutorial_k3", 18 },
        { "tutorial_k4", 19 },
        { "tutorial_k5", 20 },
        { "tutorial_a1", 21 },
        { "tutorial_a2", 22 },
        { "tutorial_a3", 23 },
        { "tutorial_a4", 24 },
        { "tutorial_a5", 25 },
        { "tutorial_p1", 26 },
        { "tutorial_p2", 27 },
        { "tutorial_p3", 28 },
        { "tutorial_p4", 29 },
        { "tutorial_p5", 30 },
    };
}
