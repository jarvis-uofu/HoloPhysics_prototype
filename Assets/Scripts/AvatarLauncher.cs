﻿using UnityEngine;
using System.Collections;

public class AvatarLauncher : MonoBehaviour {

	GameObject avatar;
	GameObject goal;
	GameObject arrow;
	Vector3 vec;

	Rigidbody rb;

	// Use this for initialization
	void Start () {		
		vec = new Vector3(1f,1f,1f);
	}

	// Update is called once per frame
	void Update () {
	}

	void OnSelect() {
		if (!this.GetComponent<Rigidbody>()) {
			var rigidBody = this.gameObject.AddComponent<Rigidbody>();
			rigidBody.collisionDetectionMode = CollisionDetectionMode.Continuous;
			rigidBody.velocity = new Vector3(2f, 0, 0f);		
		}
	}

	void OnTriggerEnter(Collider other) {
		var rigidBody = this.GetComponent<Rigidbody>();
		rigidBody.velocity = new Vector3();
		rigidBody.useGravity = false;
		if (rigidBody != null) {
			DestroyImmediate(rigidBody);
		}
		//rb.position = goal.transform.position;	
	}

	void OnCollisionEnter(Collision col){
		if (col.gameObject.name.Equals("Goal")) {
			print ("Hooray");
		}
	}
}
